package com.chrizzz35.contact.presentation

import android.content.Intent
import android.os.Bundle
import com.chrizzz35.call.CallActivity
import com.chrizzz35.contact.R
import com.chrizzz35.sms.SmsActivity
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        iv_call.setOnClickListener {
            startActivity(Intent(this, CallActivity::class.java))
        }

        iv_sms.setOnClickListener {
            startActivity(Intent(this, SmsActivity::class.java))
        }
    }
}
