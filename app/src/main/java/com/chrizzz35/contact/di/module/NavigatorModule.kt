package com.chrizzz35.contact.di.module

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import com.chrizzz35.base.constant.PhoneBookConstant.Companion.PHONE_BOOK_REQUEST_CODE
import com.chrizzz35.base.navigator.PhoneBookNavigator
import com.chrizzz35.phonebook.PhoneBookActivity
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class NavigatorModule {
    @Provides
    @Singleton
    fun providePhoneBookNavigator() : PhoneBookNavigator =
            object: PhoneBookNavigator {
                override fun showPhoneBookScreen(activity: AppCompatActivity) {
                    activity.startActivityForResult(Intent(activity, PhoneBookActivity::class.java), PHONE_BOOK_REQUEST_CODE)
                }
            }
}