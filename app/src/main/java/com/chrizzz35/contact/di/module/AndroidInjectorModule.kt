package com.chrizzz35.contact.di.module

import com.chrizzz35.base.di.scope.PerModule
import com.chrizzz35.call.CallActivity
import com.chrizzz35.call.CallModule
import com.chrizzz35.contact.presentation.MainActivity
import com.chrizzz35.contact.presentation.MainModule
import com.chrizzz35.phonebook.PhoneBookActivity
import com.chrizzz35.phonebook.PhoneBookModule
import com.chrizzz35.sms.SmsActivity
import com.chrizzz35.sms.SmsModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class AndroidInjectorModule {
    @PerModule
    @ContributesAndroidInjector(modules = [ MainModule::class ])
    abstract fun bindMainModule() : MainActivity

    @PerModule
    @ContributesAndroidInjector(modules = [ CallModule::class ])
    abstract fun bindCallModule() : CallActivity

    @PerModule
    @ContributesAndroidInjector(modules = [ SmsModule::class ])
    abstract fun bindSmsModule() : SmsActivity

    @PerModule
    @ContributesAndroidInjector(modules = [ PhoneBookModule::class ])
    abstract fun bindPhoneBookModule() : PhoneBookActivity
}