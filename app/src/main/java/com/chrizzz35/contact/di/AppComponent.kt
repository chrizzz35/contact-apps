package com.chrizzz35.contact.di

import android.app.Application
import com.chrizzz35.contact.ContactApp
import com.chrizzz35.contact.di.module.AndroidInjectorModule
import com.chrizzz35.contact.di.module.AppModule
import com.chrizzz35.contact.di.module.DatasourceModule
import com.chrizzz35.contact.di.module.NavigatorModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AppModule::class,
    DatasourceModule::class,
    NavigatorModule::class,
    AndroidInjectorModule::class,
    AndroidSupportInjectionModule::class
])
interface AppComponent : AndroidInjector<ContactApp> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application) : Builder
        fun build() : AppComponent
    }
}