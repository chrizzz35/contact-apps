package com.chrizzz35.base.constant

class PhoneBookConstant {
    companion object {
        const val PHONE_BOOK_REQUEST_CODE = 10001
        const val PHONE_BOOK_EXTRA = "phone_book_extra"
    }
}