package com.chrizzz35.base.constant

class NetworkConstant {
    companion object {
        const val API_SUCCESS = 200
        const val API_FAILED = 400
    }
}