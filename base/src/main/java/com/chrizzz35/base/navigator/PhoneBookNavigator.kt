package com.chrizzz35.base.navigator

import android.support.v7.app.AppCompatActivity

interface PhoneBookNavigator {
    fun showPhoneBookScreen(activity: AppCompatActivity)
}