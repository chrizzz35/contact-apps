package com.chrizzz35.base.domain.usecase

import io.reactivex.Flowable

interface FlowableUseCase<Param, Result> {
    fun execute(p: Param) : Flowable<Result>
}