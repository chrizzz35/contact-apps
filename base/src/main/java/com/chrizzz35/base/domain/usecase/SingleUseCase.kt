package com.chrizzz35.base.domain.usecase

import io.reactivex.Single

interface SingleUseCase<Param, Result> {
    fun execute(p: Param) : Single<Result>
}