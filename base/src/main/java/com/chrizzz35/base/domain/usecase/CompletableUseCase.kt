package com.chrizzz35.base.domain.usecase

import io.reactivex.Completable

interface CompletableUseCase<Param> {
    fun execute(p: Param) : Completable
}