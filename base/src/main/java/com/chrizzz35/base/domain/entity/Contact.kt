package com.chrizzz35.base.domain.entity

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Contact (
        val id: Int,
        val name: String,
        val phone: String
) : Parcelable