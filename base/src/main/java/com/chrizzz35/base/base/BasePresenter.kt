package com.chrizzz35.base.base

import io.reactivex.disposables.CompositeDisposable

open class BasePresenter {

    val compositeDisposable = CompositeDisposable()

    fun stop() {
        if(!compositeDisposable.isDisposed) compositeDisposable.clear()
    }
}