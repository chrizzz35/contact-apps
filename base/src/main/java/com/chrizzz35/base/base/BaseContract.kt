package com.chrizzz35.base.base

interface BaseContract {
    interface View {

    }

    interface Presenter {
        fun start()
        fun stop()
    }
}