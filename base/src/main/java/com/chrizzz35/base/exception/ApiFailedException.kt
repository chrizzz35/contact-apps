package com.chrizzz35.base.exception

class ApiFailedException(message: String?) : Exception(message)