package com.chrizzz35.sms.presentation.sms.list.state

import com.chrizzz35.sms.domain.entity.Sms

sealed class LoadSmsListState {
    class Loading : LoadSmsListState()
    class Success(val smsList: List<Sms>) : LoadSmsListState()
    class Failed(val message: String?) : LoadSmsListState()
}