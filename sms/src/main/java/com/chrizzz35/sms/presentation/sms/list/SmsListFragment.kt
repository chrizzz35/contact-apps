package com.chrizzz35.sms.presentation.sms.list

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.chrizzz35.sms.R
import com.chrizzz35.sms.adapter.SmsAdapter
import com.chrizzz35.sms.domain.entity.Sms
import com.chrizzz35.sms.presentation.sms.list.state.LoadSmsListState
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_sms_list.*
import javax.inject.Inject

private const val SMS_LIST_KEY = "SMS_LIST_KEY"

class SmsListFragment : DaggerFragment() {
    private var listener: SmsListFragmentListener? = null

    @Inject
    lateinit var smsListViewModelProvider : SmsListViewModelProvider
    private lateinit var smsListViewModel: SmsListViewModel

    private lateinit var smsAdapter: SmsAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private val smsList: MutableList<Sms> = arrayListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sms_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        smsListViewModel = ViewModelProviders.of(this, smsListViewModelProvider).get(SmsListViewModel::class.java)
        smsListViewModel.loadSmsListState.observe(this, Observer {
            when(it) {
                is LoadSmsListState.Loading -> loadSmsListLoading()
                is LoadSmsListState.Success -> loadSmsListSuccess(it.smsList)
                is LoadSmsListState.Failed -> loadSmsListFailed(it.message)
            }
        })

        layoutManager = LinearLayoutManager(context)
        smsAdapter = SmsAdapter(arrayListOf()) {
            Toast.makeText(context, "There's sms from ${it.contact.name}: ${it.message}", Toast.LENGTH_SHORT).show()
        }

        rv_sms.layoutManager = layoutManager
        rv_sms.adapter = smsAdapter

        if(savedInstanceState != null) {
            val newList: List<Sms> = savedInstanceState.getParcelableArrayList(SMS_LIST_KEY)

            this.smsList += newList
            smsAdapter.addSmsList(newList)
        }

        if(smsList.size == 0) smsListViewModel.loadSmsList()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList(SMS_LIST_KEY, ArrayList<Sms>(smsList))
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is SmsListFragmentListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement SmsListFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface SmsListFragmentListener

    companion object {
        @JvmStatic
        fun newInstance() =
                SmsListFragment().apply {
                    arguments = Bundle().apply {
                        // Arguments Setup
                    }
                }
    }

    private fun loadSmsListLoading() {
        pb_loading.visibility = View.VISIBLE
        rv_sms.visibility = View.GONE
        tv_message.visibility = View.GONE
    }

    private fun loadSmsListSuccess(smsList: List<Sms>) {
        pb_loading.visibility = View.GONE
        rv_sms.visibility = View.VISIBLE
        tv_message.visibility = View.GONE

        this.smsList += smsList
        smsAdapter.addSmsList(smsList)
    }

    private fun loadSmsListFailed(message: String?) {
        pb_loading.visibility = View.GONE
        rv_sms.visibility = View.GONE
        tv_message.visibility = View.VISIBLE

        tv_message.text = message
    }
}
