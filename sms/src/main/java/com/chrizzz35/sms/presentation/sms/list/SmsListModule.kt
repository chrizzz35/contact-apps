package com.chrizzz35.sms.presentation.sms.list

import com.chrizzz35.base.di.scope.PerScreen
import com.chrizzz35.sms.domain.usecase.GetSmsList
import dagger.Module
import dagger.Provides

@Module
class SmsListModule {
    @Provides
    @PerScreen
    fun provideSmsListViewModelProvider(getSmsList: GetSmsList) = SmsListViewModelProvider(getSmsList)
}