package com.chrizzz35.sms.presentation.sms.list

import com.chrizzz35.base.base.BaseViewModel
import com.chrizzz35.base.util.SingleLiveEvent
import com.chrizzz35.sms.domain.usecase.GetSmsList
import com.chrizzz35.sms.presentation.sms.list.state.LoadSmsListState

class SmsListViewModel (private val getSmsList: GetSmsList) : BaseViewModel() {

    val loadSmsListState = SingleLiveEvent<LoadSmsListState>()

    fun loadSmsList() {
        compositeDisposable.add(
                getSmsList.execute(GetSmsList.Param())
                        .doOnSubscribe { loadSmsListState.value = LoadSmsListState.Loading() }
                        .subscribe({
                            loadSmsListState.value = LoadSmsListState.Success(it)
                        }, {
                            loadSmsListState.value = LoadSmsListState.Failed(it.message)
                        })
        )
    }
}