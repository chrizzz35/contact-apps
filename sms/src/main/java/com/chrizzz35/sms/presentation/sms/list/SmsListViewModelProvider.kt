package com.chrizzz35.sms.presentation.sms.list

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.chrizzz35.sms.domain.usecase.GetSmsList

class SmsListViewModelProvider (private val getSmsList: GetSmsList) : ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T = SmsListViewModel(getSmsList) as T
}