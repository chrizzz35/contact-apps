package com.chrizzz35.sms.domain.entity

import android.os.Parcelable
import com.chrizzz35.base.domain.entity.Contact
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Sms (
        val id: Int,
        val contact: Contact,
        val message: String
) : Parcelable