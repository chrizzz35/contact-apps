package com.chrizzz35.sms.domain.usecase

import com.chrizzz35.base.domain.usecase.SingleUseCase
import com.chrizzz35.sms.data.SmsDatasource
import com.chrizzz35.sms.domain.entity.Sms
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetSmsList @Inject constructor(private val smsDatasource: SmsDatasource) : SingleUseCase<GetSmsList.Param, List<Sms>> {
    class Param

    override fun execute(p: Param): Single<List<Sms>> {
        return smsDatasource.getSmsList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}