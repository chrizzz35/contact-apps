package com.chrizzz35.sms.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chrizzz35.sms.R
import com.chrizzz35.sms.domain.entity.Sms
import kotlinx.android.synthetic.main.item_sms.view.*

class SmsAdapter(
        private val smsList: MutableList<Sms>,
        private val onSmsSelected: (Sms) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
            SmsViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_sms, parent, false))

    override fun getItemCount(): Int = smsList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if(viewHolder is SmsViewHolder) {
            viewHolder.bind(smsList[position], onSmsSelected)
        }
    }

    fun addSmsList(smsList: List<Sms>) {
        val count = this.smsList.size
        this.smsList += smsList

        if(count == 0) notifyDataSetChanged()
        else notifyItemRangeInserted(count, smsList.size)
    }

    class SmsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(sms: Sms, onSmsSelected: (Sms) -> Unit) {
            with(sms) {
                itemView.tv_name.text = contact.name
                itemView.tv_phone.text = contact.phone
                itemView.tv_message.text = message
                itemView.setOnClickListener { onSmsSelected(this) }
            }
        }
    }
}