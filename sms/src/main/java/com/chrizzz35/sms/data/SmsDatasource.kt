package com.chrizzz35.sms.data

import com.chrizzz35.sms.domain.entity.Sms
import io.reactivex.Single

interface SmsDatasource {
    fun getSmsList() : Single<List<Sms>>
}