package com.chrizzz35.sms.data.remote.response

import com.chrizzz35.sms.domain.entity.Sms
import com.google.gson.annotations.SerializedName

data class GetSmsListResponse (
        @SerializedName("code") val code: Int?,
        @SerializedName("message") val message: String?,
        @SerializedName("data") val smsList: List<Sms>?
)