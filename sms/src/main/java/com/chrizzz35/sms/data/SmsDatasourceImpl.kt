package com.chrizzz35.sms.data

import android.util.Log
import com.chrizzz35.base.di.named.Local
import com.chrizzz35.base.di.named.Remote
import com.chrizzz35.sms.domain.entity.Sms
import io.reactivex.Single
import javax.inject.Inject

class SmsDatasourceImpl @Inject constructor(
        @Local private val smsLocalDatasource: SmsDatasource,
        @Remote private val smsRemoteDatasource: SmsDatasource
) : SmsDatasource {

    init {
        Log.e("SmsDatasource", "SmsDatasource Created")
    }

    override fun getSmsList() : Single<List<Sms>> {
        return smsRemoteDatasource.getSmsList()
    }
}