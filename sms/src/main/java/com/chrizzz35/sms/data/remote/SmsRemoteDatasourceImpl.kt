package com.chrizzz35.sms.data.remote

import com.chrizzz35.base.constant.NetworkConstant
import com.chrizzz35.base.domain.entity.Contact
import com.chrizzz35.base.exception.ApiFailedException
import com.chrizzz35.sms.data.SmsDatasource
import com.chrizzz35.sms.domain.entity.Sms
import com.chrizzz35.sms.data.remote.response.GetSmsListResponse
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class SmsRemoteDatasourceImpl @Inject constructor() : SmsDatasource {

    override fun getSmsList(): Single<List<Sms>> {
        val dummyList = arrayListOf<Sms>()
        for (x in 0 .. 20) {
            dummyList.add(Sms(x, Contact(x, "User $x", "081234567890"), "Lorem Ipsum is simply dummy text of the printing and typesetting industry. "))
        }

        val dummyResponse = GetSmsListResponse(NetworkConstant.API_SUCCESS, "", dummyList)

        return Single.just(dummyResponse)
                .delay(5, TimeUnit.SECONDS)
                .flatMap {
                    when(it.code) {
                        NetworkConstant.API_FAILED -> throw ApiFailedException(it.message)
                        else -> Single.just(it.smsList)
                    }
                }
    }
}