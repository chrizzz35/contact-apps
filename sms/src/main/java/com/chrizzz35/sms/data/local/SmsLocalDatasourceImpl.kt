package com.chrizzz35.sms.data.local

import com.chrizzz35.sms.data.SmsDatasource
import com.chrizzz35.sms.domain.entity.Sms
import io.reactivex.Single
import javax.inject.Inject

class SmsLocalDatasourceImpl @Inject constructor() : SmsDatasource {
    override fun getSmsList(): Single<List<Sms>> {
        throw UnsupportedOperationException()
    }
}