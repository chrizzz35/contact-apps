package com.chrizzz35.sms

import com.chrizzz35.base.di.named.Local
import com.chrizzz35.base.di.named.Remote
import com.chrizzz35.base.di.scope.PerModule
import com.chrizzz35.base.di.scope.PerScreen
import com.chrizzz35.sms.data.SmsDatasource
import com.chrizzz35.sms.data.SmsDatasourceImpl
import com.chrizzz35.sms.data.local.SmsLocalDatasourceImpl
import com.chrizzz35.sms.data.remote.SmsRemoteDatasourceImpl
import com.chrizzz35.sms.presentation.sms.list.SmsListFragment
import com.chrizzz35.sms.presentation.sms.list.SmsListModule
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class SmsModule {
    @Binds
    @PerModule
    abstract fun bindSmsDatasource(SmsDatasourceImpl: SmsDatasourceImpl) : SmsDatasource

    @Local
    @Binds
    @PerModule
    abstract fun bindSmsLocalDatasource(SmsLocalDatasourceImpl: SmsLocalDatasourceImpl) : SmsDatasource

    @Remote
    @Binds
    @PerModule
    abstract fun bindSmsRemoteDatasource(SmsRemoteDatasourceImpl: SmsRemoteDatasourceImpl) : SmsDatasource

    @PerScreen
    @ContributesAndroidInjector(modules = [ SmsListModule::class ])
    abstract fun bindListSmsModule() : SmsListFragment
}