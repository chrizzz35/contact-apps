package com.chrizzz35.sms

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import com.chrizzz35.base.constant.PhoneBookConstant
import com.chrizzz35.base.domain.entity.Contact
import com.chrizzz35.base.navigator.PhoneBookNavigator
import com.chrizzz35.sms.presentation.sms.list.SmsListFragment
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class SmsActivity : DaggerAppCompatActivity(), SmsListFragment.SmsListFragmentListener {

    @Inject
    lateinit var phoneBookNavigator: PhoneBookNavigator

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sms)

        if(savedInstanceState == null) {
            val callListFragment = SmsListFragment.newInstance()
            supportFragmentManager.beginTransaction()
                    .add(R.id.fl_content, callListFragment)
                    .commitNow()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when(item?.itemId) {
            R.id.menu_phonebook -> {
                phoneBookNavigator.showPhoneBookScreen(this)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if(resultCode == Activity.RESULT_OK) {
            if(requestCode == PhoneBookConstant.PHONE_BOOK_REQUEST_CODE) {
                data?.let {
                    if(data.hasExtra(PhoneBookConstant.PHONE_BOOK_EXTRA)) {
                        val contact: Contact = data.extras.getParcelable(PhoneBookConstant.PHONE_BOOK_EXTRA)

                        Toast.makeText(this, "${contact.name} - ${contact.phone}", Toast.LENGTH_SHORT).show()
                    }
                }
            }
        }
    }
}