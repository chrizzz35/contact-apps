package com.chrizzz35.sms.presentation

import android.arch.core.executor.testing.InstantTaskExecutorRule
import android.arch.lifecycle.Observer
import com.chrizzz35.sms.data.SmsDatasource
import com.chrizzz35.sms.domain.usecase.GetSmsList
import com.chrizzz35.sms.presentation.sms.list.SmsListViewModel
import com.chrizzz35.sms.presentation.sms.list.state.LoadSmsListState
import com.chrizzz35.sms.rule.RxSchedulerRule
import com.chrizzz35.sms.util.SmsTestUtil
import io.reactivex.Single
import org.junit.*
import org.junit.rules.TestRule
import org.mockito.ArgumentCaptor
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class SmsListViewModelTest {

    @get:Rule
    val rxRule: TestRule = RxSchedulerRule()

    @get:Rule
    val liveDataRule: TestRule = InstantTaskExecutorRule()

    @Mock
    lateinit var smsDatasource: SmsDatasource
    @Mock
    lateinit var loadSmsListState: Observer<LoadSmsListState>

    lateinit var smsListViewModel: SmsListViewModel
    lateinit var getSmsList: GetSmsList

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        getSmsList = GetSmsList(smsDatasource)
        smsListViewModel = SmsListViewModel(getSmsList)
    }

    @After
    fun tearDown() {

    }

    @Test
    fun `Load sms list success`() {
        // Given
        Mockito.`when`(smsDatasource.getSmsList()).thenReturn(Single.just(SmsTestUtil.MOCK_SMS_LIST))
        smsListViewModel.loadSmsListState.observeForever(loadSmsListState)

        // When
        smsListViewModel.loadSmsList()

        // Then
        val argumentCaptor = ArgumentCaptor.forClass(LoadSmsListState::class.java)
        with(argumentCaptor) {
            Mockito.verify(loadSmsListState, Mockito.times(2)).onChanged(capture())
            val (loadingState, successState) = allValues

            Assert.assertTrue(loadingState is LoadSmsListState.Loading)
            Assert.assertTrue(successState is LoadSmsListState.Success)
            Assert.assertEquals((successState as LoadSmsListState.Success).smsList, SmsTestUtil.MOCK_SMS_LIST)
        }
    }

    @Test
    fun `Load sms list failed`() {
        // Given
        Mockito.`when`(smsDatasource.getSmsList()).thenReturn(Single.error(SmsTestUtil.API_FAILED_EXCEPTION))
        smsListViewModel.loadSmsListState.observeForever(loadSmsListState)

        // When
        smsListViewModel.loadSmsList()

        // Then
        val argumentCaptor = ArgumentCaptor.forClass(LoadSmsListState::class.java)
        with(argumentCaptor) {
            Mockito.verify(loadSmsListState, Mockito.times(2)).onChanged(capture())
            val (loadingState, successState) = allValues

            Assert.assertTrue(loadingState is LoadSmsListState.Loading)
            Assert.assertTrue(successState is LoadSmsListState.Failed)
            Assert.assertEquals((successState as LoadSmsListState.Failed).message, SmsTestUtil.API_FAILED_EXCEPTION.message)
        }
    }
}