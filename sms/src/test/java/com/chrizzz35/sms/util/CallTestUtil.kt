package com.chrizzz35.sms.util

import com.chrizzz35.base.domain.entity.Contact
import com.chrizzz35.base.exception.ApiFailedException
import com.chrizzz35.sms.domain.entity.Sms

object SmsTestUtil {

    val MOCK_SMS = Sms(1, Contact(1, "User 1", "081234567890"), "Lorem Ipsum is simply dummy text of the printing and typesetting industry. ")

    val MOCK_SMS_LIST = arrayListOf(
            MOCK_SMS,
            MOCK_SMS,
            MOCK_SMS,
            MOCK_SMS,
            MOCK_SMS,
            MOCK_SMS,
            MOCK_SMS,
            MOCK_SMS,
            MOCK_SMS,
            MOCK_SMS
    )

    val API_FAILED_EXCEPTION = ApiFailedException("EXCEPTION_MESSAGE")
}