package com.chrizzz35.phonebook.data

import com.chrizzz35.base.domain.entity.Contact
import io.reactivex.Single

interface PhoneBookDatasource {
    fun getPhoneBookList() : Single<List<Contact>>
}