package com.chrizzz35.phonebook.data

import com.chrizzz35.base.di.named.Local
import com.chrizzz35.base.di.named.Remote
import com.chrizzz35.base.domain.entity.Contact
import io.reactivex.Single
import javax.inject.Inject

class PhoneBookDatasourceImpl @Inject constructor(
        @Local private val phoneBookLocalDatasource: PhoneBookDatasource,
        @Remote private val phoneBookRemoteDatasource: PhoneBookDatasource
) : PhoneBookDatasource {
    override fun getPhoneBookList(): Single<List<Contact>> {
        return phoneBookLocalDatasource.getPhoneBookList()
    }
}