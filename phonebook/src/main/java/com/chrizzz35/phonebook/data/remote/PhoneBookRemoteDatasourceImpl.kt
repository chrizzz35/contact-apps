package com.chrizzz35.phonebook.data.remote

import com.chrizzz35.base.domain.entity.Contact
import com.chrizzz35.phonebook.data.PhoneBookDatasource
import io.reactivex.Single
import javax.inject.Inject

class PhoneBookRemoteDatasourceImpl @Inject constructor() : PhoneBookDatasource {
    override fun getPhoneBookList(): Single<List<Contact>> {
        throw UnsupportedOperationException()
    }
}