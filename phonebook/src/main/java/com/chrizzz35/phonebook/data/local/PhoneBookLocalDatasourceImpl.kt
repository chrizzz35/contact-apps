package com.chrizzz35.phonebook.data.local

import com.chrizzz35.base.domain.entity.Contact
import com.chrizzz35.phonebook.data.PhoneBookDatasource
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class PhoneBookLocalDatasourceImpl @Inject constructor() : PhoneBookDatasource {
    override fun getPhoneBookList(): Single<List<Contact>> {
        val dummyList = arrayListOf<Contact>()
        for (x in 0 .. 20) {
            dummyList.add(Contact(x, "Contact $x", "081234567890"))
        }

        return Single.just(dummyList.toList())
                .delay(5, TimeUnit.SECONDS)
    }
}