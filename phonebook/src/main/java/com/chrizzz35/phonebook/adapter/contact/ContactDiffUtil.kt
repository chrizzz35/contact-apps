package com.chrizzz35.phonebook.adapter.contact

import android.support.v7.util.DiffUtil
import com.chrizzz35.base.domain.entity.Contact

class ContactDiffUtil (
        private val oldList: List<Contact>,
        private val newList: List<Contact>
) : DiffUtil.Callback() {
    override fun areItemsTheSame(oldPosition: Int, newPosition: Int): Boolean =
            oldList[oldPosition].id == newList[newPosition].id

    override fun getOldListSize(): Int =
            oldList.size

    override fun getNewListSize(): Int =
            newList.size

    override fun areContentsTheSame(oldPosition: Int, newPosition: Int): Boolean =
            oldList[oldPosition].name == newList[newPosition].name
}