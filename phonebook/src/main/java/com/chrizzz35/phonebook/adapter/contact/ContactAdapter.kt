package com.chrizzz35.phonebook.adapter.contact

import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chrizzz35.base.domain.entity.Contact
import com.chrizzz35.phonebook.R
import kotlinx.android.synthetic.main.item_phone_book.view.*

class ContactAdapter(
        private val contactList: MutableList<Contact>,
        private val onContactSelected: (Contact) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
            ContactViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_phone_book, parent, false))

    override fun getItemCount(): Int = contactList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if(viewHolder is ContactViewHolder) {
            viewHolder.bind(contactList[position], onContactSelected)
        }
    }

    fun addContactList(contactList: List<Contact>) {
        val count = this.contactList.size
        this.contactList += contactList

        if(count == 0) notifyDataSetChanged()
        else notifyItemRangeInserted(count, contactList.size)
    }

    fun sortAscending() {
        val newList = contactList.toMutableList()
        newList.sortBy { it.id }

        val diffResult = DiffUtil.calculateDiff(ContactDiffUtil(contactList, newList))

        this.contactList.clear()
        this.contactList += newList

        diffResult.dispatchUpdatesTo(this)
    }

    fun sortDescending() {
        val newList = contactList.toMutableList()
        newList.sortByDescending { it.id }

        val diffResult = DiffUtil.calculateDiff(ContactDiffUtil(contactList, newList))

        this.contactList.clear()
        this.contactList += newList

        diffResult.dispatchUpdatesTo(this)
    }

    class ContactViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(contact: Contact, onContactSelected: (Contact) -> Unit) {
            with(contact) {
                itemView.tv_name.text = name
                itemView.tv_phone.text = phone
                itemView.setOnClickListener { onContactSelected(this) }
            }
        }
    }
}