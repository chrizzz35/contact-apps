package com.chrizzz35.phonebook

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import com.chrizzz35.base.constant.PhoneBookConstant.Companion.PHONE_BOOK_EXTRA
import com.chrizzz35.base.domain.entity.Contact
import com.chrizzz35.phonebook.presentation.phonebook.list.PhoneBookListFragment
import com.chrizzz35.phonebook.presentation.phonebook.sort.PhoneBookSortFragment
import dagger.android.support.DaggerAppCompatActivity

class PhoneBookActivity : DaggerAppCompatActivity(), PhoneBookListFragment.PhoneBookListFragmentListener,
PhoneBookSortFragment.PhoneBookSortFragmentListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_phone_book)

        if(savedInstanceState == null) {
            val phoneBookListFragment = PhoneBookListFragment.newInstance()

            supportFragmentManager.beginTransaction()
                    .replace(R.id.fl_content, phoneBookListFragment)
                    .commitNow()
        }
    }

    override fun onContactSelected(contact: Contact) {
        setResult(Activity.RESULT_OK, Intent().apply {
            putExtra(PHONE_BOOK_EXTRA, contact)
        })
        finish()
    }

    override fun showSort() {
        val phoneBookSortFragment = PhoneBookSortFragment.newInstance()
        phoneBookSortFragment.show(supportFragmentManager, PhoneBookSortFragment::class.java.name)
    }

    override fun onAscendingSelected() {
        val fragment = supportFragmentManager.findFragmentById(R.id.fl_content)
        if(fragment is PhoneBookListFragment) {
            fragment.sortAscending()
        }
    }

    override fun onDescendingSelected() {
        val fragment = supportFragmentManager.findFragmentById(R.id.fl_content)
        if(fragment is PhoneBookListFragment) {
            fragment.sortDescending()
        }
    }

    override fun showFilter() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}
