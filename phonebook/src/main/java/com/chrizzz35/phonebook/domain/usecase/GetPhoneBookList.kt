package com.chrizzz35.phonebook.domain.usecase

import com.chrizzz35.base.domain.entity.Contact
import com.chrizzz35.base.domain.usecase.SingleUseCase
import com.chrizzz35.phonebook.data.PhoneBookDatasource
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetPhoneBookList @Inject constructor(
        private val phoneBookDatasource: PhoneBookDatasource
) : SingleUseCase<GetPhoneBookList.Param, List<Contact>> {
    class Param

    override fun execute(p: Param): Single<List<Contact>> {
        return phoneBookDatasource.getPhoneBookList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}