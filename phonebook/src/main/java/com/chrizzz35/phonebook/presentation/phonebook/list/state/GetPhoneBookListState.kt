package com.chrizzz35.phonebook.presentation.phonebook.list.state

import com.chrizzz35.base.domain.entity.Contact

sealed class GetPhoneBookListState {
    class Loading : GetPhoneBookListState()
    class Success(val contactList: List<Contact>) : GetPhoneBookListState()
    class Failed(val message: String?) : GetPhoneBookListState()
}