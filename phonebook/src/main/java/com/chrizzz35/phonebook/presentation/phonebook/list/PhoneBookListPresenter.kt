package com.chrizzz35.phonebook.presentation.phonebook.list

import android.util.Log
import com.chrizzz35.phonebook.domain.usecase.GetPhoneBookList
import com.chrizzz35.phonebook.presentation.phonebook.list.state.GetPhoneBookListState

class PhoneBookListPresenter (
        private val phoneBookListViewModel: PhoneBookListViewModel,
        private val getPhoneBookList: GetPhoneBookList
) {
    fun loadContactList() {
        Log.e("Presenter", "Load Contact List")

        if(phoneBookListViewModel.getPhoneBookListState.value !is GetPhoneBookListState.Loading) {
            phoneBookListViewModel.compositeDisposable.add(
                    getPhoneBookList.execute(GetPhoneBookList.Param())
                            .doOnSubscribe { phoneBookListViewModel.getPhoneBookListState.value = GetPhoneBookListState.Loading() }
                            .subscribe({
                                Log.e("Presenter", "Load Contact List Success")
                                phoneBookListViewModel.getPhoneBookListState.value = GetPhoneBookListState.Success(it)
                            }, {
                                Log.e("Presenter", "Load Contact List Failed")
                                phoneBookListViewModel.getPhoneBookListState.value = GetPhoneBookListState.Failed(it.message)
                            })
            )
        }
    }
}