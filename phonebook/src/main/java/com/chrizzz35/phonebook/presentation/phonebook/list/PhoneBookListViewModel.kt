package com.chrizzz35.phonebook.presentation.phonebook.list

import com.chrizzz35.base.base.BaseViewModel
import com.chrizzz35.base.util.SingleLiveEvent
import com.chrizzz35.phonebook.presentation.phonebook.list.state.GetPhoneBookListState

class PhoneBookListViewModel : BaseViewModel() {
    val getPhoneBookListState = SingleLiveEvent<GetPhoneBookListState>()
}