package com.chrizzz35.phonebook.presentation.phonebook.list

import android.arch.lifecycle.Observer
import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chrizzz35.base.domain.entity.Contact
import com.chrizzz35.phonebook.R
import com.chrizzz35.phonebook.adapter.contact.ContactAdapter
import com.chrizzz35.phonebook.presentation.phonebook.list.state.GetPhoneBookListState
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_phone_book_list.*
import javax.inject.Inject

private const val CONTACT_LIST_KEY = "contact_list_key"

class PhoneBookListFragment : DaggerFragment() {

    private var listener: PhoneBookListFragmentListener? = null

    @Inject lateinit var phoneBookListViewModel: PhoneBookListViewModel
    @Inject lateinit var phoneBookListPresenter: PhoneBookListPresenter

    private lateinit var layoutManager: LinearLayoutManager
    private lateinit var contactAdapter: ContactAdapter
    private val contactList: MutableList<Contact> = arrayListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_phone_book_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        phoneBookListViewModel.getPhoneBookListState.observe(this, Observer {
            when(it) {
                is GetPhoneBookListState.Loading -> getPhoneBookListLoading()
                is GetPhoneBookListState.Success -> getPhoneBookListSuccess(it.contactList)
                is GetPhoneBookListState.Failed -> getPhoneBookListFailed(it.message)
            }
        })

        layoutManager = LinearLayoutManager(context)
        contactAdapter = ContactAdapter(arrayListOf()) {
            listener?.onContactSelected(it)
        }

        rv_contact.layoutManager = layoutManager
        rv_contact.adapter = contactAdapter

        btn_filter.setOnClickListener {
            listener?.showFilter()
        }
        btn_sort.setOnClickListener {
            listener?.showSort()
        }

        if(savedInstanceState != null) {
            val newList: List<Contact> = savedInstanceState.getParcelableArrayList(CONTACT_LIST_KEY)

            this.contactList += newList
            contactAdapter.addContactList(newList)
        }

        if(contactList.size == 0) phoneBookListPresenter.loadContactList()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList(CONTACT_LIST_KEY, ArrayList<Contact>(contactList))
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is PhoneBookListFragmentListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement PhoneBookListFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface PhoneBookListFragmentListener {
        fun onContactSelected(contact: Contact)
        fun showSort()
        fun showFilter()
    }

    companion object {
        @JvmStatic
        fun newInstance() =
                PhoneBookListFragment().apply {
                    arguments = Bundle().apply {
                        // Arguments Setup
                    }
                }
    }

    private fun getPhoneBookListLoading() {
        pb_loading.visibility = View.VISIBLE
        rv_contact.visibility = View.GONE
        tv_message.visibility = View.GONE
        btn_filter.visibility = View.GONE
        btn_sort.visibility = View.GONE
    }

    private fun getPhoneBookListSuccess(contactList: List<Contact>) {
        pb_loading.visibility = View.GONE
        rv_contact.visibility = View.VISIBLE
        tv_message.visibility = View.GONE
        btn_filter.visibility = View.VISIBLE
        btn_sort.visibility = View.VISIBLE

        this.contactList += contactList
        contactAdapter.addContactList(contactList)
    }

    private fun getPhoneBookListFailed(message: String?) {
        pb_loading.visibility = View.GONE
        rv_contact.visibility = View.GONE
        tv_message.visibility = View.VISIBLE
        btn_filter.visibility = View.GONE
        btn_sort.visibility = View.GONE

        tv_message.text = message
    }

    fun sortAscending() {
        contactAdapter.sortAscending()
    }

    fun sortDescending() {
        contactAdapter.sortDescending()
    }
}
