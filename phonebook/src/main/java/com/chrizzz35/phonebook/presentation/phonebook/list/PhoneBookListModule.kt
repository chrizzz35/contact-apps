package com.chrizzz35.phonebook.presentation.phonebook.list

import android.arch.lifecycle.ViewModelProviders
import com.chrizzz35.base.di.scope.PerScreen
import com.chrizzz35.phonebook.domain.usecase.GetPhoneBookList
import dagger.Module
import dagger.Provides

@Module
class PhoneBookListModule {
    @Provides
    @PerScreen
    fun providePhoneBookListViewModel(phoneBookListFragment: PhoneBookListFragment) : PhoneBookListViewModel =
            ViewModelProviders.of(phoneBookListFragment).get(PhoneBookListViewModel::class.java)

    @Provides
    @PerScreen
    fun providePhoneBookListPresenter(phoneBookListViewModel: PhoneBookListViewModel, getPhoneBookList: GetPhoneBookList) : PhoneBookListPresenter =
            PhoneBookListPresenter(phoneBookListViewModel, getPhoneBookList)
}