package com.chrizzz35.phonebook.presentation.phonebook.sort

import android.content.Context
import android.os.Bundle
import android.support.design.widget.BottomSheetDialogFragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chrizzz35.phonebook.R
import kotlinx.android.synthetic.main.fragment_phone_book_sort.*

class PhoneBookSortFragment : BottomSheetDialogFragment() {

    private var listener: PhoneBookSortFragmentListener? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_phone_book_sort, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        iv_asc.setOnClickListener {
            listener?.onAscendingSelected()
            dismiss()
        }
        iv_desc.setOnClickListener {
            listener?.onDescendingSelected()
            dismiss()
        }
        btn_close.setOnClickListener {
            dismiss()
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is PhoneBookSortFragmentListener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement PhoneBookSortFragmentListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
    interface PhoneBookSortFragmentListener {
        fun onAscendingSelected()
        fun onDescendingSelected()
    }

    companion object {
        @JvmStatic
        fun newInstance() =
                PhoneBookSortFragment().apply {
                    arguments = Bundle().apply {
                        // Arguments Setup
                    }
                }
    }
}
