package com.chrizzz35.phonebook

import com.chrizzz35.base.di.named.Local
import com.chrizzz35.base.di.named.Remote
import com.chrizzz35.base.di.scope.PerModule
import com.chrizzz35.base.di.scope.PerScreen
import com.chrizzz35.phonebook.data.PhoneBookDatasource
import com.chrizzz35.phonebook.data.PhoneBookDatasourceImpl
import com.chrizzz35.phonebook.data.local.PhoneBookLocalDatasourceImpl
import com.chrizzz35.phonebook.data.remote.PhoneBookRemoteDatasourceImpl
import com.chrizzz35.phonebook.presentation.phonebook.list.PhoneBookListFragment
import com.chrizzz35.phonebook.presentation.phonebook.list.PhoneBookListModule
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class PhoneBookModule {
    @Binds
    @PerModule
    abstract fun bindPhoneBookDatasource(phoneBookDatasourceImpl: PhoneBookDatasourceImpl) : PhoneBookDatasource

    @Local
    @Binds
    @PerModule
    abstract fun bindPhoneBookLocalDatasource(phoneBookLocalDatasourceImpl: PhoneBookLocalDatasourceImpl) : PhoneBookDatasource

    @Remote
    @Binds
    @PerModule
    abstract fun bindPhoneBookRemoteDatasource(phoneBookRemoteDatasourceImpl: PhoneBookRemoteDatasourceImpl) : PhoneBookDatasource

    @PerScreen
    @ContributesAndroidInjector(modules = [ PhoneBookListModule::class ])
    abstract fun bindPhoneBookListModule() : PhoneBookListFragment
}