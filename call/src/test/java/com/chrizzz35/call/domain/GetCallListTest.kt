package com.chrizzz35.call.domain

import com.chrizzz35.call.data.CallDatasource
import com.chrizzz35.call.domain.usecase.GetCallList
import com.chrizzz35.call.rule.RxSchedulerRule
import com.chrizzz35.call.util.CallTestUtil
import io.reactivex.Single
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations

class GetCallListTest {

    @get:Rule
    val rxRule : TestRule = RxSchedulerRule()

    @Mock
    lateinit var callDatasource: CallDatasource
    lateinit var getCallList: GetCallList

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        getCallList = GetCallList(callDatasource)
    }

    @After
    fun tearDown() {

    }

    @Test
    fun `Execute usecase success`() {
        // Given
        Mockito.`when`(callDatasource.getCallList()).thenReturn(Single.just(CallTestUtil.MOCK_CALL_LIST))

        // When
        val testObserver = getCallList.execute(GetCallList.Param()).test()
        testObserver.awaitTerminalEvent()

        // Then
        testObserver.assertValue(CallTestUtil.MOCK_CALL_LIST)
    }

    @Test
    fun `Execute usecase failed`() {
        // Given
        Mockito.`when`(callDatasource.getCallList()).thenReturn(Single.error(CallTestUtil.API_FAILED_EXCEPTION))

        // When
        val testObserver = getCallList.execute(GetCallList.Param()).test()
        testObserver.awaitTerminalEvent()

        // Then
        testObserver.assertError(CallTestUtil.API_FAILED_EXCEPTION)
    }
}