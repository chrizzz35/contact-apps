package com.chrizzz35.call.util

import com.chrizzz35.base.domain.entity.Contact
import com.chrizzz35.base.exception.ApiFailedException
import com.chrizzz35.call.domain.entity.Call

object CallTestUtil {

    val MOCK_CALL = Call(1, Contact(1, "User 1", "081234567890"), "2017-01-01 00:00:00")

    val MOCK_CALL_LIST = arrayListOf(
            MOCK_CALL,
            MOCK_CALL,
            MOCK_CALL,
            MOCK_CALL,
            MOCK_CALL,
            MOCK_CALL,
            MOCK_CALL,
            MOCK_CALL,
            MOCK_CALL,
            MOCK_CALL
    )

    val API_FAILED_EXCEPTION = ApiFailedException("EXCEPTION_MESSAGE")
}