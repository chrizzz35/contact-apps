package com.chrizzz35.call.presentation.call

import com.chrizzz35.call.data.CallDatasource
import com.chrizzz35.call.domain.usecase.GetCallList
import com.chrizzz35.call.presentation.call.list.CallListContract
import com.chrizzz35.call.presentation.call.list.CallListPresenter
import com.chrizzz35.call.rule.RxSchedulerRule
import com.chrizzz35.call.util.CallTestUtil
import io.reactivex.Single
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.Mockito.inOrder
import org.mockito.MockitoAnnotations

class CallListPresenterTest {

    @get:Rule
    val rxRule : TestRule = RxSchedulerRule()

    @Mock
    lateinit var callDatasource: CallDatasource
    @Mock
    lateinit var view: CallListContract.View

    lateinit var presenter: CallListContract.Presenter
    lateinit var getCallList: GetCallList

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)

        getCallList = GetCallList(callDatasource)
        presenter = CallListPresenter(view, getCallList)
    }

    @After
    fun tearDown() {

    }

    @Test
    fun `Load call list success`() {
        // When
        Mockito.`when`(callDatasource.getCallList()).thenReturn(Single.just(CallTestUtil.MOCK_CALL_LIST))

        // Given
        presenter.loadCallList()

        // Then
        val inOrder = inOrder(view)
        inOrder.verify(view).loadCallListLoading()
        inOrder.verify(view).loadCallListSuccess(CallTestUtil.MOCK_CALL_LIST)
    }

    @Test
    fun `Load call list failed`() {
        // When
        Mockito.`when`(callDatasource.getCallList()).thenReturn(Single.error(CallTestUtil.API_FAILED_EXCEPTION))

        // Given
        presenter.loadCallList()

        // Then
        val inOrder = inOrder(view)
        inOrder.verify(view).loadCallListLoading()
        inOrder.verify(view).loadCallListFailed(CallTestUtil.API_FAILED_EXCEPTION.message)
    }
}