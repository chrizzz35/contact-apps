package com.chrizzz35.call.data

import com.chrizzz35.call.domain.entity.Call
import io.reactivex.Single

interface CallDatasource {
    fun getCallList() : Single<List<Call>>
}