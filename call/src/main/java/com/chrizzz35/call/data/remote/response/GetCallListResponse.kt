package com.chrizzz35.call.data.remote.response

import com.chrizzz35.call.domain.entity.Call
import com.google.gson.annotations.SerializedName

data class GetCallListResponse (
        @SerializedName("code") val code: Int?,
        @SerializedName("message") val message: String?,
        @SerializedName("data") val callList: List<Call>?
)