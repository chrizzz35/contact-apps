package com.chrizzz35.call.data.local

import com.chrizzz35.call.data.CallDatasource
import com.chrizzz35.call.domain.entity.Call
import io.reactivex.Single
import javax.inject.Inject

class CallLocalDatasourceImpl @Inject constructor() : CallDatasource {
    override fun getCallList(): Single<List<Call>> {
        throw UnsupportedOperationException()
    }
}