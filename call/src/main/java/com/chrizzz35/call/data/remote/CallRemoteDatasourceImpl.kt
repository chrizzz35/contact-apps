package com.chrizzz35.call.data.remote

import com.chrizzz35.base.constant.NetworkConstant
import com.chrizzz35.base.domain.entity.Contact
import com.chrizzz35.base.exception.ApiFailedException
import com.chrizzz35.call.data.CallDatasource
import com.chrizzz35.call.domain.entity.Call
import com.chrizzz35.call.data.remote.response.GetCallListResponse
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class CallRemoteDatasourceImpl @Inject constructor() : CallDatasource {

    override fun getCallList(): Single<List<Call>> {
        val dummyList = arrayListOf<Call>()
        for (x in 0 .. 20) {
            dummyList.add(Call(x, Contact(x, "User $x", "081234567890"), "2017-01-01 00:00:00"))
        }

        val dummyResponse = GetCallListResponse(NetworkConstant.API_SUCCESS, "", dummyList)

        return Single.just(dummyResponse)
                .delay(5, TimeUnit.SECONDS)
                .flatMap {
                    when(it.code) {
                        NetworkConstant.API_FAILED -> throw ApiFailedException(it.message)
                        else -> Single.just(it.callList)
                    }
                }
    }
}