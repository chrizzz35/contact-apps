package com.chrizzz35.call.data

import android.util.Log
import com.chrizzz35.base.di.named.Local
import com.chrizzz35.base.di.named.Remote
import com.chrizzz35.call.domain.entity.Call
import io.reactivex.Single
import javax.inject.Inject

class CallDatasourceImpl @Inject constructor(
        @Local private val callLocalDatasource: CallDatasource,
        @Remote private val callRemoteDatasource: CallDatasource
) : CallDatasource {

    init {
        Log.e("CallDatasource", "CallDatasource Created")
    }

    override fun getCallList() : Single<List<Call>> {
        return callRemoteDatasource.getCallList()
    }
}