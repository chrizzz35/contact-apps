package com.chrizzz35.call.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.chrizzz35.call.R
import com.chrizzz35.call.domain.entity.Call
import kotlinx.android.synthetic.main.item_call.view.*

class CallAdapter(
        private val callList: MutableList<Call>,
        private val onCallSelected: (Call) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder =
            CallViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_call, parent, false))

    override fun getItemCount(): Int = callList.size

    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {
        if(viewHolder is CallViewHolder) {
            viewHolder.bind(callList[position], onCallSelected)
        }
    }

    fun addCallList(callList: List<Call>) {
        val count = this.callList.size
        this.callList += callList

        if(count == 0) notifyDataSetChanged()
        else notifyItemRangeInserted(count, callList.size)
    }

    class CallViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(call: Call, onCallSelected: (Call) -> Unit) {
            with(call) {
                itemView.tv_name.text = contact.name
                itemView.tv_phone.text = contact.phone
                itemView.tv_time.text = time
                itemView.setOnClickListener { onCallSelected(this) }
            }
        }
    }
}