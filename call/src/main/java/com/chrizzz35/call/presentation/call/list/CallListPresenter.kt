package com.chrizzz35.call.presentation.call.list

import com.chrizzz35.base.base.BasePresenter
import com.chrizzz35.call.domain.usecase.GetCallList

class CallListPresenter (
        private val view: CallListContract.View,
        private val getCallList: GetCallList
) : BasePresenter(), CallListContract.Presenter {

    override fun start() {
        if(!view.isCallListExist()) {
            loadCallList()
        }
    }

    override fun loadCallList() {
        compositeDisposable.add(
                getCallList.execute(GetCallList.Param())
                        .doOnSubscribe { view.loadCallListLoading() }
                        .subscribe({
                            view.loadCallListSuccess(it)
                        }, {
                            view.loadCallListFailed(it.message)
                        })
        )
    }
}