package com.chrizzz35.call.presentation.call.list

import com.chrizzz35.base.base.BaseContract
import com.chrizzz35.call.domain.entity.Call

interface CallListContract {
    interface View : BaseContract.View {
        fun isCallListExist() : Boolean
        fun loadCallListLoading()
        fun loadCallListSuccess(callList: List<Call>)
        fun loadCallListFailed(message: String?)
    }

    interface Presenter : BaseContract.Presenter {
        fun loadCallList()
    }
}