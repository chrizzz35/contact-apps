package com.chrizzz35.call.presentation.call.list

import com.chrizzz35.base.di.scope.PerScreen
import com.chrizzz35.call.domain.usecase.GetCallList
import dagger.Module
import dagger.Provides

@Module
class CallListModule {
    @Provides
    @PerScreen
    fun bindListCallPresenter(callListFragment: CallListFragment, getCallList: GetCallList) : CallListContract.Presenter =
            CallListPresenter(callListFragment, getCallList)
}