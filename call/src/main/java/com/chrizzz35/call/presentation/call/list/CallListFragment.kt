package com.chrizzz35.call.presentation.call.list

import android.content.Context
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.chrizzz35.call.R
import com.chrizzz35.call.adapter.CallAdapter
import com.chrizzz35.call.domain.entity.Call
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_call_list.*
import javax.inject.Inject

private const val CALL_LIST_KEY = "CALL_LIST_KEY"

class CallListFragment : DaggerFragment(), CallListContract.View {
    private var listener: Listener? = null

    @Inject
    lateinit var presenter: CallListContract.Presenter

    private lateinit var callAdapter: CallAdapter
    private lateinit var layoutManager: LinearLayoutManager
    private val callList: MutableList<Call> = arrayListOf()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_call_list, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        layoutManager = LinearLayoutManager(context)
        callAdapter = CallAdapter(arrayListOf()) {
            Toast.makeText(context, "There's call from ${it.contact.name} at ${it.time}", Toast.LENGTH_SHORT).show()
        }

        rv_call.layoutManager = layoutManager
        rv_call.adapter = callAdapter

        if(savedInstanceState != null) {
            val newList: List<Call> = savedInstanceState.getParcelableArrayList(CALL_LIST_KEY)

            callList += newList
            callAdapter.addCallList(newList)
        }
    }

    override fun onResume() {
        super.onResume()
        presenter.start()
    }

    override fun onPause() {
        super.onPause()
        presenter.stop()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelableArrayList(CALL_LIST_KEY, ArrayList<Call>(callList))
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is Listener) {
            listener = context
        } else {
            throw RuntimeException(context.toString() + " must implement Listener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }

    interface Listener

    companion object {
        @JvmStatic
        fun newInstance() =
                CallListFragment().apply {
                    arguments = Bundle().apply {
                        // Arguments Setup
                    }
                }
    }

    override fun isCallListExist() : Boolean = callList.size > 0

    override fun loadCallListLoading() {
        pb_loading.visibility = View.VISIBLE
        rv_call.visibility = View.GONE
        tv_message.visibility = View.GONE
    }

    override fun loadCallListSuccess(callList: List<Call>) {
        pb_loading.visibility = View.GONE
        rv_call.visibility = View.VISIBLE
        tv_message.visibility = View.GONE

        this.callList += callList
        callAdapter.addCallList(callList)
    }

    override fun loadCallListFailed(message: String?) {
        pb_loading.visibility = View.GONE
        rv_call.visibility = View.GONE
        tv_message.visibility = View.VISIBLE

        tv_message.text = message
    }
}
