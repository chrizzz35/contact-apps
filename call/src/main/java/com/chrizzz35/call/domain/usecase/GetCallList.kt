package com.chrizzz35.call.domain.usecase

import com.chrizzz35.base.domain.usecase.SingleUseCase
import com.chrizzz35.call.data.CallDatasource
import com.chrizzz35.call.domain.entity.Call
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GetCallList @Inject constructor(private val callDatasource: CallDatasource) : SingleUseCase<GetCallList.Param, List<Call>> {
    class Param

    override fun execute(p: Param): Single<List<Call>> {
        return callDatasource.getCallList()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
    }
}