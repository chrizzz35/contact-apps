package com.chrizzz35.call.domain.entity

import android.os.Parcelable
import com.chrizzz35.base.domain.entity.Contact
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Call (
        val id: Int,
        val contact: Contact,
        val time: String
) : Parcelable