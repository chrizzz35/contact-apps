package com.chrizzz35.call

import com.chrizzz35.base.di.named.Local
import com.chrizzz35.base.di.named.Remote
import com.chrizzz35.base.di.scope.PerModule
import com.chrizzz35.base.di.scope.PerScreen
import com.chrizzz35.call.data.CallDatasource
import com.chrizzz35.call.data.CallDatasourceImpl
import com.chrizzz35.call.data.local.CallLocalDatasourceImpl
import com.chrizzz35.call.data.remote.CallRemoteDatasourceImpl
import com.chrizzz35.call.presentation.call.list.CallListFragment
import com.chrizzz35.call.presentation.call.list.CallListModule
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class CallModule {
    @Binds
    @PerModule
    abstract fun bindCallDatasource(callDatasourceImpl: CallDatasourceImpl) : CallDatasource

    @Local
    @Binds
    @PerModule
    abstract fun bindCallLocalDatasource(callLocalDatasourceImpl: CallLocalDatasourceImpl) : CallDatasource

    @Remote
    @Binds
    @PerModule
    abstract fun bindCallRemoteDatasource(callRemoteDatasourceImpl: CallRemoteDatasourceImpl) : CallDatasource

    @PerScreen
    @ContributesAndroidInjector(modules = [ CallListModule::class ])
    abstract fun bindListCallModule() : CallListFragment
}